var cytoscape = require('cytoscape');
var cose_bilkent = require('cytoscape-cose-bilkent');

cytoscape.use(cose_bilkent);

fetch('data.json', {mode: 'no-cors'})
  .then(function(res) {
    return res.json()
  })
  .then(function(data) {
    var size = function(node) {
        return 10 + Math.min(10, node.degree());
    }

    var cy = window.cy = cytoscape({
      container: document.getElementById('cy'),

      layout: {
        name: 'random',
        fit: true
      },

      style: [
        {
          selector: 'node',
          style: {
            'label': 'data(label)',
            'width': size,
            'height': size,
            'font-size': size,
            'background-color': '#ff0000'
          }
        },

        {
          selector: ':parent',
          style: {
            'font-size': 40,
            'background-opacity': 0.333
          }
        },

        {
          selector: 'edge',
          style: {
            'width': 1,
            'line-color': '#ad1a66'
          }
        }
      ],

      elements: data
    });

    // Indo-European subset
    var focus = cy.$('#ine-pro');
    var component = cy.elements().componentsOf(focus)[0];
    var componentParents = component.parent();

    var collection = cy.filter(function(element, i) {
        return !component.contains(element) &&
               !componentParents.contains(element);
    });

    cy.remove(collection);
    cy.ready(function() {
       cy.layout({
           name: 'cose-bilkent',
           fit: false,
           animate: 'during',
           nodeDimensionsIncludeLabels: true,
           stop: function() {
              cy.fit(focus.neighborhood());
           }
       }).run();
    });

    cy.$('node').on('tap', function(event) {
        var node = event.target;
        var label = node.data('label');
        var url = 'https://en.wiktionary.org/wiki/Category:'+label+' language';
        if (node.isParent()) {
            url += 's';
        }
        var win = window.open(url, '_blank');
        win.focus();

        return false;
    });
  });
